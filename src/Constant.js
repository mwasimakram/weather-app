export const GET_ALL_CITIES="GET_ALL_CITIES";
export const SET_CURRENT_WEATHER = "SET_CURRENT_WEATHER"
export const LIMIT = 30;
export const START_INDEX = 0;
export const WEEK_DAYS = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
export const INPUT_SEARCH_LIMIT_MSG = "You can't enter the more than 5 digit";
export const INPUT_SEARCH_REGEX_MSG= "Sorry only digit allowed"

//Routes constant

export const DETAILS_PAGE= "/forcast-details";


//paths

export const GET_CITIES_URL = "https://raw.githubusercontent.com/millbj92/US-Zip-Codes-JSON/master/USCities.json";
export const BASE_URL = "http://api.weatherapi.com/v1/forecast.json?key=4ce7481ec118414a81b135806222806";