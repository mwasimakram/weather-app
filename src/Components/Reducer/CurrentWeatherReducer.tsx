export function CurrentWeatherReducer(state:any,action:any){
    const {type,payload} =  action;
    switch(type){
        case 'SET_CURRENT_WEATHER':
            return{
                current:payload
            }
        case 'SET_WEATHER_LOCATION':
            return{
                location:payload
            }
        case 'SET_WEATHER_FORCAST':
            return{
                forcast:payload
            }
    }
}