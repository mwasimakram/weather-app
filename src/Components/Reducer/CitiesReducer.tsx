import { GET_ALL_CITIES, SET_CURRENT_WEATHER } from "../../Constant";
const initialState = {
  citiesName: [],
};

export function CitiesWeatherReducer(state = initialState, action: any) {
  const { type, payload } = action;
  switch (type) {
    case GET_ALL_CITIES:
      return {
        ...state,
        citiesName: payload ? payload : initialState,
      };
    case SET_CURRENT_WEATHER:
      return {
        ...state,
        weather: payload,
      };
  }
}
