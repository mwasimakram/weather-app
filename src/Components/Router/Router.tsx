import { Routes, Route } from "react-router-dom";
import { ForcastDetails } from "../Weather/ForcastDetails";
import { Home } from "../Weather/Home";
export const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />}></Route>
      <Route path="forcast-details" element={<ForcastDetails />}></Route>
    </Routes>
  );
};
