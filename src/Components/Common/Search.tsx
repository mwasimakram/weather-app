import { useCallback, useState } from "react";
import { debounce } from "lodash";
import {INPUT_SEARCH_LIMIT_MSG,INPUT_SEARCH_REGEX_MSG} from "../../Constant";
export function Search(props: any) {
  const { searchCity } = props;
  const [error, setError] = useState(" ");
  const setQueryValue = (event) => {

    const numberRegEx = /^[0-9]+$/;
    const isNumber = numberRegEx.test(String(event.target.value).toLowerCase());

    if (!isNumber && event.target.value) {
      setError(INPUT_SEARCH_REGEX_MSG);
      return false;
    }

    if (event.target.value.length > 5) {
      setError(INPUT_SEARCH_LIMIT_MSG);
      return false;
    }

    if (isNumber && event.target.value) {
      setError(" ");
    }

    searchCity(event.target.value);

  };

  const delayHandler = useCallback(debounce(setQueryValue, 300), []);

  return (
    <div className="container row col-md-4">
      <input
        placeholder="Enter zip code / city name"
        onChange={delayHandler}
      ></input>
      {error && <p style={{ color: "red" }}>{error}</p>}
    </div>
  );
}
