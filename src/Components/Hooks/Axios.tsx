import { configure } from "axios-hooks";
import axios from "axios";
import { makeUseAxios } from "axios-hooks";
import {BASE_URL} from "../../Constant";
export const useAxios = makeUseAxios({
  axios: axios.create({
    baseURL: BASE_URL,
  }),
});

configure({ axios });
