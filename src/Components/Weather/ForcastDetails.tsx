import { useLocation } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { weatherContext } from "../Context/Cities";
import { useAxios } from "../Hooks/Axios";
import { WEEK_DAYS, SET_CURRENT_WEATHER } from "../../Constant";
import "./index.css";
export function ForcastDetails() {
  const [isCel, setCel] = useState(true);
  const [isCompactView, setCompactView] = useState(false);
  const location = useLocation();
  const cityName: any = location.state;
  const { state, dispatch } = useContext(weatherContext);
  const [{ loading }, getData] = useAxios({
    url: "",
    params: { q: cityName, days: 4, aqi: "no" },
  });
  let forcastData = [];

  const forcast = async () => {
    const data = await getData();
    forcastData = data.data;
    dispatch({ type: SET_CURRENT_WEATHER, payload: forcastData });
  };

  const getDate = (d: string) => {
    const dt = new Date(d);
    const currentDay = dt.toString().split(" ")[0];
    return currentDay;
  };

  const getWeekDay = (d: string) => {
    const dt = new Date(d);
    const time = dt.getHours() >= 12 ? "PM" : "AM";
    const day = WEEK_DAYS[dt.getDay()];
    return day + " " + d.split(" ")[1] + time;
  };

  const changeView = () => {
    setCompactView(!isCompactView);
  };
  useEffect(() => {
    forcast();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-1"></div>
        <div className="col-lg-10">
          <div className="card">
            <div className="card-header">
              <div className="form-check form-switch">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="flexSwitchCheckDefault"
                  onChange={() => changeView()}
                />
                <label className="form-check-label">{isCompactView ? 'Detail View' : 'Compact View'}</label>
              </div>
            </div>
            <div className="card-title justify-content-center">
              {state.weather && (
                <div className="card-body card-bg">
                  <div className="row">
                    <div className="col-md-7 d-flex">
                      <div className="col-md-1">
                        {state.weather && (
                          <img
                            src={state.weather.current.condition.icon}
                            alt=""
                          ></img>
                        )}
                      </div>
                      <div className="col-md-3 d-flex">
                        {state.weather && (
                          <h2 className="col-md-8 ps-4 pt-2">
                            {isCel
                              ? state.weather.current.temp_c
                              : state.weather.current.temp_f}
                          </h2>
                        )}
                        <span
                          className={
                            "col-md-1 fontSize " +
                            (isCel ? "active" : "grayClr")
                          }
                          onClick={() => setCel(true)}
                        >
                          C
                        </span>
                        <span className="col-md-1 fontSize grayClr text-center">
                          |
                        </span>
                        <span
                          className={
                            "col-md-1 fontSize " +
                            (!isCel ? "active" : "grayClr")
                          }
                          onClick={() => setCel(false)}
                        >
                          F
                        </span>
                      </div>
                      <div className="col-md-3 grayClr ps-3">
                        <p className="removeBottomMargin">
                          Precipitation {state.weather.current.precip_in}%
                        </p>
                        <p className="removeBottomMargin">
                          Humidity {state.weather.current.humidity}%
                        </p>
                        <p>Wind {state.weather.current.wind_kph} km/h</p>
                      </div>
                    </div>
                    <div className="col-md-5">
                      <div className="text-end">
                        <h4>
                          {state.weather.location.name}{" "}
                          {state.weather.location.country}
                        </h4>
                      </div>
                      <div className="text-end">
                        <p className="removeBottomMargin grayClr">
                          {getWeekDay(state.weather.location.localtime)}
                        </p>
                      </div>
                      <div className="text-end">
                        <p className=" grayClr ">
                          {state.weather.current.condition.text}
                        </p>
                      </div>
                    </div>
                  </div>
                  {!isCompactView && (
                    <>
                      <div className="row col-md-4 mb-3">
                        <div
                          className="btn-group"
                          role="group"
                          aria-label="Basic example"
                        >
                          <button type="button" className="tabBtn activeBtn">
                            Temprature
                          </button>{" "}
                          |
                          <button type="button" className="tabBtn">
                            Middle
                          </button>{" "}
                          |
                          <button type="button" className="tabBtn">
                            Right
                          </button>
                        </div>
                      </div>
                      <div className="row text-center">
                        <div className="col-md-1 grayClr justify-content-center fs-6">
                          11 AM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          12 AM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          1 PM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          2 PM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          3 PM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          4 PM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          5 PM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          6 PM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          7 PM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          8 PM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          9 PM
                        </div>
                        <div className="col-md-1 grayClr justify-content-center">
                          10 PM
                        </div>
                      </div>
                      <div className="row ">
                        {state.weather.forecast.forecastday.map(
                          (obj: any, index) => (
                            <div className="col-md-1 justify-content-center">
                              <p className="removeBottomMargin text-center">
                                {getDate(obj.date)}
                              </p>
                              <img src={obj.day.condition.icon} alt=""></img>
                              <p className="text-center fs-6">
                                <span className="tmpFontsize">
                                  {isCel
                                    ? obj.day.maxtemp_c
                                    : obj.day.maxtemp_f}
                                </span>{" "}
                                <span className="grayClr tmpFontsize">
                                  {isCel
                                    ? obj.day.mintemp_c
                                    : obj.day.mintemp_f}
                                </span>
                              </p>
                            </div>
                          )
                        )}
                      </div>
                    </>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="col-lg-1"></div>
      </div>
    </div>
  );
}

// {
//     obj.hour.map((h,i)=>(
//         <div className="col-sm-1">
//             <p>{getDate(h.time)}</p>
//             <img src={h.condition.icon} alt=""></img>
//             <p>{h.temp_c} </p>
//         </div>
//     ))
// }
