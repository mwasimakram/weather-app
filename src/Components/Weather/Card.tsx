import { useContext, useEffect, useMemo, useState } from "react";
import { weatherContext } from "../Context/Cities";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import { LIMIT, START_INDEX,DETAILS_PAGE,GET_ALL_CITIES ,GET_CITIES_URL} from "../../Constant";
interface Iprops {
  searchQuery: string;
}
export function Cards(props: Iprops) {
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);

  let renderCities = [];
  let totalRcd = 0;

  let nevigate = useNavigate();

  const { searchQuery } = props;
  const { state, dispatch } = useContext(weatherContext);

  const fetchData = async () => {
    let d = await axios.get(GET_CITIES_URL);
    dispatch({ type: GET_ALL_CITIES, payload: d.data });
    setLoading(false);
  };

  let citiesList = state.citiesName;
  totalRcd = citiesList && citiesList.length;

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  renderCities = useMemo(loadCities, [citiesList, page, searchQuery]);

  function loadCities() {
    if (searchQuery) {
      return citiesList.filter((o) =>
        o.zip_code.toString().includes(searchQuery)
      );
    }

    const endIndex = LIMIT * page;
    return citiesList.slice(START_INDEX, endIndex);
  }

  function MoveToDetails(city: string) {
    nevigate(DETAILS_PAGE, { state: city, replace: false });
  }
  return (
    <>
      {loading && <div className="card">Loading...</div>}
      <div className="card text-end mt-3">{`Showing ${renderCities.length} entreis of ${totalRcd}`}</div>

      <InfiniteScroll
        dataLength={renderCities.length}
        next={() => setPage(page + 1)}
        hasMore={true}
        loader={true}
      >
        <div className="row d-flex">
          {renderCities &&
            renderCities.map((value, index) => (
              <div id="card1" className="col-md-3 card ml-2 mt-1 cursor">
                <div
                  className="card-body"
                  onClick={() => MoveToDetails(value.city)}
                >
                  {value.city}
                </div>
              </div>
            ))}
        </div>
      </InfiniteScroll>
    </>
  );
}
