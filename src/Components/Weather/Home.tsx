import { Cards } from "./Card";
import { useState } from "react";
import { Search } from "../Common/Search";
export function Home() {
  const [searchQuery, setSearchQuery] = useState("");
  const searchCity = (value) => {
    setSearchQuery(value);
  };
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-lg-1"></div>
        <div className="col-lg-10 card">
          <div className="card-header text-center">Weather Forcast</div>
          <div className="card-body">
            <div className="d-flex justify-content-center mb-3">
              <Search searchCity={searchCity} />
            </div>
            <div className="card">
              <div className="row d-flex card-body ">
                <Cards searchQuery={searchQuery} />
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-1"></div>
      </div>
    </div>
  );
}
