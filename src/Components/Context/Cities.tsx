import { createContext, useEffect, useReducer } from "react";
import { CitiesWeatherReducer } from "../Reducer/CitiesReducer";

const initialState = {
  citiesName: [],
};
export const weatherContext = createContext(null);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const WeatherContextProvider = (props: any) => {
  const [state, dispatch] = useReducer(CitiesWeatherReducer, initialState);
  return (
    <weatherContext.Provider value={{ state, dispatch }}>
      {props.children}
    </weatherContext.Provider>
  );
};
export default WeatherContextProvider;
