import React from "react";
import "./App.css";
import { Router } from "../src/Components/Router/Router";
import { Headers } from "./Components/Common/Header";
import  WeatherContextProvider  from "./Components/Context/Cities";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import './Components/Hooks/Axios'
function App() {
  return (
    <>
      <WeatherContextProvider>
        <Headers />
        <Router />
      </WeatherContextProvider>
    </>
  );
}

export default App;
